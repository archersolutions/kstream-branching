package com.sonali.kafkaproducer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ThreadLocalRandom;


@Component
public class RandomNumberProducer {

    private static final int MIN = 10;
    private static final int MAX = 100_000;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Scheduled(fixedRate = 1000)
    public void produce() throws UnknownHostException, JsonProcessingException {
//        int random = ThreadLocalRandom.current().nextInt(MIN, MAX);
//        String json_msg = "{\n" +
//                "    \"phone_number\": \"123\",\n" +
//                "    \"status\" : \"block\"\n" +
//                "}";

        String json_msg = getMessage();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(json_msg);
        this.kafkaTemplate.send(new ProducerRecord<>("json_source","1", actualObj));

//        this.kafkaTemplate.sendDefault(msg);
        //just for logging
        String hostName = InetAddress.getLocalHost().getHostName();
        System.out.println(String.format("%s produced %s", hostName, json_msg));
    }

    private String getMessage() {
        long ts = System.currentTimeMillis();
        if(ts%2==0) return "{\"phone_number\": \"123\",\"status\" : \"block\"}";

        else return "{\"phone_number\": \"123\",\"status\" : \"unblock\"}";
    }

}