/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kafka.streams.branching;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Function;

@SpringBootApplication
public class KafkaStreamsBranchingSample {

	public static void main(String[] args) {
		SpringApplication.run(KafkaStreamsBranchingSample.class, args);
	}

	public static class WordCountProcessorApplication {

		@Bean
		@SuppressWarnings("unchecked")
		public Function<KStream<String, JsonNode>, KStream<String, JsonNode>[]> process() {

			Predicate<String, JsonNode> isBlock = (k, v) -> {
				String status = v.get("status").asText();
				System.out.println("****************"+status);
				return status.equals("block");
			};
			Predicate<String, JsonNode> isUnblock = (k, v) -> v.get("status").asText().equals("unblock");

			return input -> input
					.branch(isBlock, isUnblock);
		}

//		@Bean
//		@SuppressWarnings("unchecked")
//		public Function<KStream<Object, String>, KStream<?, WordCount>[]> process() {
//
//			Predicate<Object, WordCount> isEnglish = (k, v) -> v.word.equals("english");
//			Predicate<Object, WordCount> isFrench =  (k, v) -> v.word.equals("french");
//			Predicate<Object, WordCount> isSpanish = (k, v) -> v.word.equals("spanish");
//
//			return input -> input
//					.flatMapValues(value -> Arrays.asList(value.toLowerCase().split("\\W+")))
//					.groupBy((key, value) -> value)
//					.windowedBy(TimeWindows.of(Duration.ofSeconds(6)))
//					.count(Materialized.as("WordCounts-1"))
//					.toStream()
//					.map((key, value) -> new KeyValue<>(null,
//							new WordCount(key.key(), value, new Date(key.window().start()), new Date(key.window().end()))))
//					.branch(isEnglish, isFrench, isSpanish);
//		}
	}
}
